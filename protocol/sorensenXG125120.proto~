Terminator  = CR;

ReadTimeout   = 1000;
WriteTimeout  = 1000;
PollPeriod    = 100;
ReplyTimeout  = 2000;
ExtraInput    = Ignore;


###
# Init ... Enable interlocks
##
enableIntlk
{
    out "SENS:PROT:INT %s";
}

getIntlk
{
    out "SENS:PROT:INT?";
    in  "%d";
}

###
# Reset
##
reset 
{
    out "*RST";
}

###
# Power
##
power_cmd 
{
    out "OUTP %s";
}

get_status
{
    out "OUTP:STAT?";
    in  "%d";
}

###
# Local/Remote
##
loc_rem_mode
{
    out "SYST:REM:STAT %s";
}

get_loc_rem_mode
{
    out "SYST:REM:STAT?";
    in  "%{Front Panel Locked|Front Panel Unlocked}";
}

###
# Current
##
get_current
{
    out "SOUR:CURR?";
    in  "%f";
}

set_current 
{
    out "SOUR:CURR %f";
    @init { get_current };
}

measure_current
{
    out "MEAS:CURR?";
    in  "%f";
}

###
# Voltage
##
get_voltage 
{
    out "SOUR:VOLT?";
    in  "%f";
}

set_voltage 
{
    out "SOUR:VOLT %f";
    @init { get_voltage };
}

measure_voltage 
{
    out "MEAS:VOLT?";
    in  "%f";
}

###
# Diagnostics
##

# Should not fail, return 0 if OK.
simple_test
{
    out "*TST?";
    in  "%d";
}

get_ovp
{
    out "SOUR:VOLT:PROT?";
    in  "%f";
}

set_ovp
{
    out "SOUR:VOLT:PROT %f";
    @init {
        get_ovp;
    };
}

get_ovp_error
{
    out "SOUR:VOLT:PROT:TRIP?";
    in  "%d";
}

###
# Errors
##

get_statusbyte
{
    out "*STB?";
    in  "%d";
}

get_next_error
{
    out "SYST:ERR?";
    in  "%d";
}

clear_error
{
    out "*CLS";
}
