program change_polarity_seq

%% #include <string.h>
%% #include <stdio.h>
%% #include <math.h>
%% #include <stdarg.h>

%{#include <stdlib.h>}%
%{#include <unistd.h>}%

option +d;

/* State machine info PV */
#define INITIAL 0
#define POS_POLARITY 1
#define SET_ZERO 2
#define WAIT_ZERO 3
#define NEGATIVE_CMD 4
#define WAIT_NEG_PLC 5
#define SET_NEG_VAL 6
#define NEG_POLARITY 7
#define POSITIVE_CMD 8
#define WAIT_POS_PLC 9
#define SET_POS_VAL 10

/* Current readback PV */
float current_rb;
assign current_rb to "{secsub}:{disdevidxps}:CurR-Hw";
monitor current_rb;

/* Current setpoint PV */
float current_sp;
assign current_sp to "{secsub}:{disdevidxps}:CurS";
monitor current_sp;
evflag curFlag;
sync current_sp curFlag;

/* PLC polarity readback */
float polarity_rb;
assign polarity_rb to "{secsub}:{disdevidxplc}:PolR";
monitor polarity_rb;

/* PLC positive polarity setpoint */
float pospol_sp;
assign pospol_sp to "{secsub}:{disdevidxplc}:PolPosCmd";

/* PLC negative polarity setpoint */
float negpol_sp;
assign negpol_sp to "{secsub}:{disdevidxplc}:PolNegCmd";

/* Current hw setpoint PV */
float current_hw;
assign current_hw to "{secsub}:{disdevidxps}:CurS-Hw";

/* State machine status PV */
int state_info;
assign state_info to "{secsub}:{disdevidxps}:stateS";

ss auto_polarity {
	

	/************************** INITIAL STATE *******************************************/
	/* Checks the PLC to see what is current polarity of the PS                         */
	/*                                                                                  */ 
	/************************************************************************************/
	
	state init_state {

		entry {
			state_info = INITIAL;
			pvPut(state_info);
			pvGet(polarity_rb);
		}
		
		when (polarity_rb == 1) {}
		state pos_polarity

		when (polarity_rb == 0) {}
		state neg_polarity
	}

	

	/*********************** STATE POSITIVE POLARITY ***********************************/
	/* Waits for a new setpoint command                                                */
	/* if the command is a positive number, just write (send serial command) normally  */
	/* if it's a negative number, start the sequence to change polarity                */ 
	/***********************************************************************************/

	state pos_polarity {
		option -e;
	
		entry {
			state_info = POS_POLARITY;
			pvPut(state_info);
		}
	
		when ((current_sp >= 0.0) && efTestAndClear(curFlag)) {
			current_hw = current_sp;
			pvPut(current_hw);
		}
		state pos_polarity

		when ((current_sp < 0.0) && efTestAndClear(curFlag)) {}
		state pos_neg_1

		when(delay(10)) {}
		state init_state
	}



	/*********************** STATE POSITIVE to NEGATIVE 1 ******************************/
	/* Simply write PS setpoint to be ZERO and move to next state                      */
	/*                                                                                 */
	/*                                                                                 */ 
	/***********************************************************************************/
	
	state pos_neg_1 {
		entry {
			state_info = SET_ZERO;
			pvPut(state_info);
		}
		when() {
			current_hw = 0.0;
			pvPut(current_hw);
		}
		state pos_neg_2
	}



	/*********************** STATE POSITIVE to NEGATIVE 2 ******************************/
	/* Wait until the output of the power supply is between -10 mA and +10 mA          */
	/* to guarantee that is close to zero, then move to next state                     */
	/*                                                                                 */ 
	/***********************************************************************************/

	state pos_neg_2 {
		entry {
			state_info = WAIT_ZERO;
			pvPut(state_info);
		}
		when((current_rb <= 0.1) && (current_rb >= -0.1)) {}
		state pos_neg_3

		when(delay(10)) {}
		state init_state
	}



	/*********************** STATE POSITIVE to NEGATIVE 3 ******************************/
	/* Send the command to PLC that changes the polarity from POSITIVE (0) to          */
	/* NEGATIVE (1)                                                                    */
	/*                                                                                 */ 
	/***********************************************************************************/

	state pos_neg_3 {
		entry {
			state_info = NEGATIVE_CMD;
			pvPut(state_info);
		}
		when() {
			pospol_sp = 1;
			pvPut(pospol_sp);		
		}
		state pos_neg_4
	}



	/*********************** STATE POSITIVE to NEGATIVE 4 ******************************/
	/* Waits until the readback PV from the PLC confirms that it is NEGATIVE (0)       */
	/*                                                                                 */
	/*                                                                                 */ 
	/***********************************************************************************/

	state pos_neg_4 {
		entry {
			state_info = WAIT_NEG_PLC;
			pvPut(state_info);
			pvGet(polarity_rb);
		}
		
		when(polarity_rb == 0) {}
		state pos_neg_5

		when(delay(10)) {}
		state init_state
	}



	/*********************** STATE POSITIVE to NEGATIVE 5 ******************************/
	/* Writes the new setpoint to the setpoint PV using its absolute (positive) value  */
	/* Move to NEGATIVE POLARITY state                                                 */
	/*                                                                                 */ 
	/***********************************************************************************/

	state pos_neg_5 {
		entry {
			state_info = SET_NEG_VAL;
			pvPut(state_info);
		}
		when() {
			current_hw = fabsf(current_sp);
			pvPut(current_hw);
		}
		state neg_polarity
	}




	/*********************** STATE NEGATIVE POLARITY ***********************************/
	/* Waits for a new setpoint command                                                */
	/* if the command is a negative number, just write (send serial command) normally  */
	/* if it's a positive number, start the sequence to change polarity                */ 
	/***********************************************************************************/

	state neg_polarity {
		option -e;

		entry {
			state_info = NEG_POLARITY;
			pvPut(state_info);
		}

		when ((current_sp >= 0.0)  && efTestAndClear(curFlag)) {}
		state neg_pos_1

		when ((current_sp < 0.0) && efTestAndClear(curFlag)) {
			current_hw = fabsf(current_sp);
			pvPut(current_hw);
		}
		state neg_polarity

		when(delay(10)) {}
		state init_state
	}



	/*********************** STATE NEGATIVE to POSITIVE 1 ******************************/
	/* Simply write PS setpoint to be ZERO and move to next state                      */
	/*                                                                                 */
	/*                                                                                 */ 
	/***********************************************************************************/
	
	state neg_pos_1 {
		entry {
			state_info = SET_ZERO;
			pvPut(state_info);
		}
		when() {
			current_hw = 0.0;
			pvPut(current_hw);
		}
		state neg_pos_2
	}



	/*********************** STATE NEGATIVE to POSITIVE 2 ******************************/
	/* Wait until the output of the power supply is between -10 mA and +10 mA          */
	/* to guarantee that is close to zero, then move to next state                     */
	/*                                                                                 */ 
	/***********************************************************************************/
	
	state neg_pos_2 {
		entry {
			state_info = WAIT_ZERO;
			pvPut(state_info);
		}
		when((current_rb <= 0.1) && (current_rb >= -0.1)) {}
		state neg_pos_3

		when(delay(10)) {}
		state init_state
	}



	/*********************** STATE NEGATIVE to POSITIVE 3 ******************************/
	/* Send the command to PLC that changes the polarity from POSITIVE (1) to          */
	/* NEGATIVE (0)                                                                    */
	/*                                                                                 */ 
	/***********************************************************************************/

	state neg_pos_3 {
		entry {
			state_info = POSITIVE_CMD;
			pvPut(state_info);
		}
		when() {
			pospol_sp = 1;
			pvPut(pospol_sp);		
		}
		state neg_pos_4
	}



	/*********************** STATE NEGATIVE to POSITIVE 4 ******************************/
	/* Waits until the readback PV from the PLC confirms that it is POSITIVE (1)       */
	/*                                                                                 */
	/*                                                                                 */ 
	/***********************************************************************************/

	state neg_pos_4 {
		entry {
			state_info = WAIT_POS_PLC;
			pvPut(state_info);
			pvGet(polarity_rb);
		}
		when(polarity_rb == 1) {}
		state neg_pos_5

		when(delay(10)) {}
		state init_state
	}



	/*********************** STATE NEGATIVE to POSITIVE 5 ******************************/
	/* Writes the new setpoint to the setpoint PV using its absolute (positive) value  */
	/* Move to POSITIVE POLARITY state                                                 */
	/*                                                                                 */ 
	/***********************************************************************************/

	state neg_pos_5 {
		entry {
			state_info = SET_POS_VAL;
			pvPut(state_info);
		}
		when() {
			current_hw = fabsf(current_sp);
			pvPut(current_hw);
		}
		state pos_polarity
	}
}

