/**
 * @brief Returns string depending on the error code.
 */

// Subroutine
#include <aSubRecord.h>

// Export Subroutine Function
#include <registryFunction.h>
#include <epicsExport.h>

// Normalized C Types
#include <stdint.h>
#include <math.h>

// Main Dependences
#include "epicsExit.h"
#include "epicsThread.h"
#include "iocsh.h"



int main(int argc, char *argv[])
{
    if (argc >= 2)
    {
        iocsh(argv[1]);
        epicsThreadSleep(.2);
    }

    iocsh(NULL);
    epicsExit(0);

    return 0;
}

static long get_error_message(aSubRecord *prec)
{
    switch((long) prec->a)
    {
        case    0: prec->vala = "No error";
                   break;

        case  201: prec->vala = "Unexpected warm boot";
                   break;

        case  102: prec->vala = "Incompatible unit type";
                   break;

        case  100: prec->vala = "Incompatibility error";
                   break;

        case -102: prec->vala = "Syntax error";
                   break;

        case -108: prec->vala = "Parameter not allowed";
                   break;

        case -151: prec->vala = "Invalid string data";
                   break;

        case -161: prec->vala = "Invalid block data";
                   break;

        case -200: prec->vala = "Execution error";
                   break;

        case -203: prec->vala = "Command protected";
                   break;

        case -221: prec->vala = "Settings conflict";
                   break;

        case -222: prec->vala = "Data out of range";
                   break;

        case -225: prec->vala = "Out of memory";
                   break;

        case -241: prec->vala = "Hardware missing";
                   break;

        case -284: prec->vala = "Program currently running";
                   break;

        case -292: prec->vala = "Referenced name does not exist";
                   break;

        case -293: prec->vala = "Referenced name already exists";
                   break;

        case -316: prec->vala = "Checksum error";
                   break;

        case -330: prec->vala = "Self-test failed";
                   break;

        case -340: prec->vala = "Calibration failed";
                   break;

        case -350: prec->vala = "Queue overflow";
                   break;

        case -360: prec->vala = "Communication error";
                   break;
    }

    return 0;
}

epicsRegisterFunction(get_error_message);


#define POSITIVE_POLARITY 1
#define NEGATIVE_POLARITY 0

/* 
  Input A is real readback value from the hardware
  Input B is polarity readback from PLC
  Output A is the bipolar current readout
*/

static int steerer_current(aSubRecord *precord) {
  float hw_curr_value;
  int polarity_value;

  hw_curr_value = *(float *)precord->a;
  polarity_value = *(int *)precord->b;

  //printf("before -> CUR = %3.3f | POL=%d\n", hw_curr_value, polarity_value);
  
  if (polarity_value == NEGATIVE_POLARITY) {
    hw_curr_value = hw_curr_value * (-1.0);
  }

  //printf("after --> CUR = %3.3f | POL=%d\n", hw_curr_value, polarity_value);

  *(float *)precord->vala = hw_curr_value;

  return 0;
}

epicsRegisterFunction(steerer_current);
