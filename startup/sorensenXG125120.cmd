require seq,2.1.10
require streamdevice,2.7.7
require synappsstd,3.5.0


drvAsynIPPortConfigure("SteerPS-H1", "172.16.60.48:5025")
drvAsynIPPortConfigure("SteerPS-V1", "172.16.60.50:5025")
drvAsynIPPortConfigure("SteerPS-H2", "172.16.60.49:5025")
drvAsynIPPortConfigure("SteerPS-V2", "172.16.60.51:5025")

dbLoadRecords("sorensenXG125120.db")

doAfterIocInit('seq change_polarity_seq, "secsub=LEBT-010, disdevidxps=PwrC-PSCH-01, disdevidxplc=BMD-CH-01"')
doAfterIocInit('seq change_polarity_seq, "secsub=LEBT-010, disdevidxps=PwrC-PSCV-01, disdevidxplc=BMD-CV-01"')
doAfterIocInit('seq change_polarity_seq, "secsub=LEBT-010, disdevidxps=PwrC-PSCH-02, disdevidxplc=BMD-CH-02"')
doAfterIocInit('seq change_polarity_seq, "secsub=LEBT-010, disdevidxps=PwrC-PSCV-02, disdevidxplc=BMD-CV-02"')


